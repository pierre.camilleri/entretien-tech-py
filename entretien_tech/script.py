from typing import List

from entretien_tech.sort import sort_by_relevance
from entretien_tech.types import Program, ProgramNature, Sector

programs: List[Program] = [
    Program(
        title="Tremplin",
        nature=ProgramNature.Funding,
        sectors=[
            Sector.Tertiary,
            Sector.Tourism,
            Sector.Craftsmanship,
            Sector.Industry,
        ],
        cost=None,
    ),
    Program(
        title="Fonds tourisme durable",
        nature=ProgramNature.Support,
        sectors=[Sector.Tourism],
        cost="Gratuit",
    ),
    Program(
        title="Prêt vert ADEME",
        nature=ProgramNature.Loan,
        sectors=[Sector.Craftsmanship, Sector.Industry],
        cost=None,
    ),
    Program(
        title="Aides au réemploi des emballages",
        nature=ProgramNature.Funding,
        sectors=[
            Sector.Tertiary,
            Sector.Tourism,
            Sector.Craftsmanship,
            Sector.Industry,
        ],
        cost=None,
    ),
    Program(
        title="Formations RSE",
        nature=ProgramNature.Training,
        sectors=[
            Sector.Tertiary,
            Sector.Tourism,
            Sector.Craftsmanship,
            Sector.Industry,
        ],
        cost="Gratuit (sauf en Bretagne et Grand Est)",
    ),
    Program(
        title="Réparacteur",
        nature=ProgramNature.Training,
        sectors=[Sector.Craftsmanship],
        cost="Gratuit",
    ),
    Program(
        title="Coup de pouce chauffage",
        nature=ProgramNature.Support,
        sectors=[Sector.Tertiary],
        cost="2000 €",
    ),
]

if __name__ == "__main__":
    sorted_programs = sort_by_relevance(programs)

    print("Liste des programmes triés :")
    print()
    print(sorted_programs)
