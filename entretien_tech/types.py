from dataclasses import dataclass
from enum import Enum
from typing import List, Optional


class ProgramNature(Enum):
    Funding = "financement"
    Support = "accompagnement"
    Training = "formation"
    Loan = "prêt"


class Sector(Enum):
    Craftsmanship = "artisanat"
    Industry = "industrie"
    Tourism = "tourisme"
    Tertiary = "tertiaire"


@dataclass
class Program:
    title: str
    nature: ProgramNature
    sectors: List[Sector]
    cost: Optional[str]
