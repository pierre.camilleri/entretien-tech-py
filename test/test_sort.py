"""
GIVEN a list of programs
WHEN I sort them by relevance
EXPECT the programs be in the expected order
"""


def test_one_plus_one():
    """One plus one equal two"""
    assert 1 + 1 == 2
